# MathP #

![MathP Logo](https://dl.dropbox.com/u/58518258/MathP/MathP%20Logo%20Script.png)

> A Mathematical Syntax for Common Lisp

Lisp has a beautiful, eminently grokkable syntax. It's great for general programming, macros and getting stuff done. However, if you've tried to write more than basic mathematics in Lisp, you'll no doubt have wanted to write things like `x+y*z` or `a*x^2 + b*x + c`. Many people who have just learned Lisp coming from other Algol based languages miss that expressiveness. They, like me, probably wanted a mathematics domain specific language (DSL). Well, now you've got one. MathP lets you write the above expressions within Common Lisp. MathP is a mathematics DSL.

MathP syntax attempts to retain a lot of the existing syntax of Common Lisp, while providing infix operators for mathematical notation. As a bonus, MathP syntax for function calls is vaguely similar to M-expressions, and there is a rudimentary macro system.

Once you have MathP loaded (see links below), try evaluating `#M f(x)=x^3 f(1+2)*10`. It should return `270`. Other things to try include `read-from-string` of a MathP expression, and `macroexpand-1`'ing a MathP expression. Many other examples may be found on [the MathP website][MathP].

MathP is released under a BSD licence.

**The MathP website is [here][MathP]. It contains examples, the latest released version, and documentation.**

**The public git repository is [here](https://bitbucket.org/Kohath/mathp/overview).**

[MathP]: http://blog.metalight.net/2012/09/mathp-math-notation-in-common-lisp.html
         "MathP"